﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;


[RequireComponent(typeof(Interactable))]
public class GrabbedDetector : MonoBehaviour
{

	private Vector3 oldPosition;
	private Quaternion oldRotation;


	private Hand.AttachmentFlags attachmentFlags = Hand.defaultAttachmentFlags & (~Hand.AttachmentFlags.SnapOnAttach) & (~Hand.AttachmentFlags.DetachOthers) & (~Hand.AttachmentFlags.VelocityMovement);

	private Interactable interactable;

	public bool isGrabbed = false;

	public GameEvent sfx_pickup;
	public GameEvent sfx_drop;

	void Awake()
	{


		interactable = this.GetComponent<Interactable>();
	}


	//-------------------------------------------------
	// Called when a Hand starts hovering over this object
	//-------------------------------------------------
	private void OnHandHoverBegin(Hand hand)
	{
		//generalText.text = "Hovering hand: " + hand.name;
	}


	//-------------------------------------------------
	// Called when a Hand stops hovering over this object
	//-------------------------------------------------
	private void OnHandHoverEnd(Hand hand)
	{
		//generalText.text = "No Hand Hovering";
	}


	//-------------------------------------------------
	// Called every Update() while a Hand is hovering over this object
	//-------------------------------------------------
	private void HandHoverUpdate(Hand hand)
	{
		GrabTypes startingGrabType = hand.GetGrabStarting();
		bool isGrabEnding = hand.IsGrabEnding(this.gameObject);

		if (interactable.attachedToHand == null && startingGrabType != GrabTypes.None)
		{
			// Save our position/rotation so that we can restore it when we detach
			oldPosition = transform.position;
			oldRotation = transform.rotation;

			// Call this to continue receiving HandHoverUpdate messages,
			// and prevent the hand from hovering over anything else
			hand.HoverLock(interactable);

			// Attach this object to the hand
			hand.AttachObject(gameObject, startingGrabType, attachmentFlags);
		}
		else if (isGrabEnding)
		{
			// Detach this object from the hand
			hand.DetachObject(gameObject);

			// Call this to undo HoverLock
			hand.HoverUnlock(interactable);

			// Restore position/rotation
			//transform.position = oldPosition;
			transform.rotation = oldRotation;
		}
	}


	//-------------------------------------------------
	// Called when this GameObject becomes attached to the hand
	//-------------------------------------------------
	private void OnAttachedToHand(Hand hand)
	{
		//generalText.text = string.Format("Attached: {0}", hand.name);
		//attachTime = Time.time;
		isGrabbed = true;
		print("grabbed");
		// Save our position/rotation so that we can restore it when we detach
		oldPosition = transform.position;
		oldRotation = transform.rotation;

		sfx_pickup.Raise();
	}



	//-------------------------------------------------
	// Called when this GameObject is detached from the hand
	//-------------------------------------------------
	private void OnDetachedFromHand(Hand hand)
	{
		//generalText.text = string.Format("Detached: {0}", hand.name);
		isGrabbed = false;
		print("released");
		//transform.position = oldPosition;
		transform.rotation = oldRotation;

		sfx_drop.Raise();

	}


	//-------------------------------------------------
	// Called every Update() while this GameObject is attached to the hand
	//-------------------------------------------------
	private void HandAttachedUpdate(Hand hand)
	{
		//generalText.text = string.Format("Attached: {0} :: Time: {1:F2}", hand.name, (Time.time - attachTime));
		//isGrabbed = true;
	}

	private bool lastHovering = false;
	private void Update()
	{
		if (interactable.isHovering != lastHovering) //save on the .tostrings a bit
		{
			//hoveringText.text = string.Format("Hovering: {0}", interactable.isHovering);
			lastHovering = interactable.isHovering;
		}
	}


	//-------------------------------------------------
	// Called when this attached GameObject becomes the primary attached object
	//-------------------------------------------------
	private void OnHandFocusAcquired(Hand hand)
	{
	}


	//-------------------------------------------------
	// Called when another attached GameObject becomes the primary attached object
	//-------------------------------------------------
	private void OnHandFocusLost(Hand hand)
	{
	}
}

