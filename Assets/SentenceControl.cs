﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class SentenceControl : MonoBehaviour
{
    public GameObject testCube1;
    public GameObject testCube2;
    public GameObject testCube3;

    public GameObject word_prefab;

    public GameObject[] hints;

    string sentence = "";
    public string newSentence;

    List<String> words = new List<string>();
    List<GameObject> wordObjects = new List<GameObject>();
    public List<int> wordOrder = new List<int>();
    List<float> wordPositions = new List<float>();

    public float spaceLength = 0.03f;
    public float movementSpeed = 0.1f;
    public float sentenceHeight = 0.6f;
    

    char[] separators = new char[] { ' ' };//, '.' };
    public string example = "This is an example sentence.";

    public Vector3 textPosRelativeToHead = new Vector3();
    Transform head;

    float completeRoutineDuration = 2f;

    public GameEvent sfx_complete;

    private WaitForEndOfFrame endOfFrame = new WaitForEndOfFrame();
    //private float hintTime = 0f;

    private List<string> SplitIntoWords(string _sentence)
    {
        string _sentence_padding = _sentence;// + " end";

        List<String> _words = new List<string>();

        foreach (var word in _sentence_padding.Split(separators, StringSplitOptions.RemoveEmptyEntries))
        {
            Debug.Log(word);
            _words.Add(word);
        }

        return _words;
    }

    void SpaunWords(List<string> _words)
    {
        print("SpaunAndPlaceWords " + _words.Count);


        //random word order to start
        wordOrder.Clear();
        for (int i = 0; i < _words.Count; i++)
            wordOrder.Add(i);
        ShuffleArray(wordOrder);

        //instantiate words
        //clear old ones
        foreach (GameObject word in wordObjects)
            Destroy(word);
        wordObjects.Clear();

        for (int i = 0; i < _words.Count; i++)
        {

            wordObjects.Add(Instantiate(word_prefab, this.transform));

            

        }

        for (int i = 0; i < _words.Count; i++)
        {
            TMP_Text text = wordObjects[i].GetComponentInChildren<TMP_Text>();


            text.text = _words[i];

            

        }

    }

    void FindWordPositions() {

        print(wordObjects.Count);

        wordPositions = ReturnWordPositions(wordObjects, wordOrder);

        
    }

    void MoveWords()
    {
        for (int i = 0; i < wordObjects.Count; i++)
        {
            //print(wordObjects[i].GetComponent<GrabbedDetector>().isGrabbed.ToString());

            if (!wordObjects[i].GetComponent<GrabbedDetector>().isGrabbed)//is grabbed
            {
                Transform trans = wordObjects[wordOrder[i]].GetComponent<Transform>();
                trans.localPosition = Vector3.Lerp(trans.localPosition, Vector3.right * wordPositions[i] + Vector3.up * sentenceHeight, movementSpeed);
                trans.SetParent(this.transform);
            }
        }
    }

    void CheckWordOrder()
    {
        //if object is grabbed, check where it falls in the order, if it is out of order, then reset positions
        for (int i = 0; i < wordObjects.Count; i++)
        {
            if (wordObjects[wordOrder[i]].GetComponent<GrabbedDetector>().isGrabbed && !ChangeBang) //is grabbed
            {
                

                float xPos = wordObjects[wordOrder[i]].transform.localPosition.x;
                print(wordOrder[i] + " " + wordPositions.Count);

                testCube1.transform.localPosition = wordObjects[wordOrder[i]].transform.localPosition;

                Vector3 pos2 = Vector3.zero;
                Vector3 pos3 = Vector3.zero;
                if(i < words.Count - 1)
                    pos2 = new Vector3(wordObjects[wordOrder[i+1]].transform.localPosition.x, testCube1.transform.localPosition.y, testCube1.transform.localPosition.z);
                if(i > 0)
                    pos3 = new Vector3(wordObjects[wordOrder[i-1]].transform.localPosition.x, testCube1.transform.localPosition.y, testCube1.transform.localPosition.z);

                testCube2.transform.localPosition = pos2;
                testCube3.transform.localPosition = pos3;

                if (i < wordObjects.Count - 1 && xPos > wordObjects[wordOrder[i + 1]].transform.localPosition.x)
                {
                    print(xPos + "  " + wordPositions[wordOrder[i + 1]]);

                    //change order of words, update positions
                    int temp = wordOrder[i];
                    wordOrder[i] = wordOrder[i + 1];
                    wordOrder[i + 1] = temp;

                    ChangeBang = true;
                    print("order changed!");
                    Invoke("ResetChangeBang", movementSpeed * 1f);
                    FindWordPositions();

                }
                //print(wordOrder[i] + " " + wordPositions.Count);

                if(!ChangeBang && i > 0 && xPos < wordObjects[wordOrder[i - 1]].transform.localPosition.x)
                {
                    print(xPos + "  " + wordPositions[wordOrder[i - 1]]);


                    int temp = wordOrder[i - 1];
                    wordOrder[i - 1] = wordOrder[i];
                    wordOrder[i] = temp;

                    ChangeBang = true;
                    print("order changed!");

                    Invoke("ResetChangeBang", movementSpeed * 1f);
                    FindWordPositions();


                }
            }
        }
    }
    /*
    void CheckWordOrder()
    {
        //if object is grabbed, check where it falls in the order, if it is out of order, then reset positions
        for (int i = 0; i < wordObjects.Count; i++)
        {
            if (wordObjects[wordOrder[i]].GetComponent<GrabbedDetector>().isGrabbed && !ChangeBang) //is grabbed
            {
                

                float xPos = wordObjects[wordOrder[i]].transform.localPosition.x;
                print(wordOrder[i] + " " + wordPositions.Count);

                testCube1.transform.localPosition = wordObjects[wordOrder[i]].transform.localPosition;

                Vector3 pos2 = Vector3.zero;
                Vector3 pos3 = Vector3.zero;
                if(i < words.Count - 1)
                    pos2 = new Vector3(wordPositions[wordOrder[i + 1]], testCube1.transform.localPosition.y, testCube1.transform.localPosition.z);
                if(i > 0)
                    pos3 = new Vector3(wordPositions[wordOrder[i - 1]], testCube1.transform.localPosition.y, testCube1.transform.localPosition.z);

                testCube2.transform.localPosition = pos2;
                testCube3.transform.localPosition = pos3;

                if (i < wordObjects.Count - 1 && xPos > wordPositions[wordOrder[i + 1]])
                {
                    print(xPos + "  " + wordPositions[wordOrder[i + 1]]);

                    //change order of words, update positions
                    int temp = wordOrder[i];
                    wordOrder[i] = wordOrder[i + 1];
                    wordOrder[i + 1] = temp;

                    ChangeBang = true;
                    print("order changed!");
                    Invoke("ResetChangeBang", movementSpeed * 2f);
                    FindWordPositions();

                }
                //print(wordOrder[i] + " " + wordPositions.Count);

                if(!ChangeBang && i > 0 && xPos < wordPositions[wordOrder[i - 1]])
                {
                    print(xPos + "  " + wordPositions[wordOrder[i - 1]]);


                    int temp = wordOrder[i - 1];
                    wordOrder[i - 1] = wordOrder[i];
                    wordOrder[i] = temp;

                    ChangeBang = true;
                    print("order changed!");

                    Invoke("ResetChangeBang", movementSpeed * 2f);
                    FindWordPositions();


                }
            }
        }
    }*/

    bool ChangeBang = false;
    void ResetChangeBang()
    {
        ChangeBang = false;
    }


    List<float> ReturnWordPositions(List<GameObject> _wordObjects, List<int> _wordOrder)
    {

        List<float> _wordPositions = new List<float>();
        float positionSum = 0;

        float xScale = wordObjects[0].GetComponentInChildren<RectTransform>().localScale.x;

        for (int i = 0; i < _wordObjects.Count; i++)
        {
            TMP_Text text = wordObjects[_wordOrder[i]].GetComponentInChildren<TMP_Text>();

            _wordPositions.Add(positionSum);

            positionSum += text.renderedWidth * xScale + spaceLength;
        }

        


        for (int i = 0; i < _wordObjects.Count; i++)
        {
            _wordPositions[i] -= positionSum/2f;
        }

        return _wordPositions;
    }
    

    void Start()
    {
        head = GameObject.Find("VRCamera").transform;
        //SetUpNewSentence(example);

        InvokeRepeating(nameof(ShowHints), 5f, 5f);
    }

    public void SetUpNewSentence(string _sentence)
    {




        this.transform.position = head.position + textPosRelativeToHead;

        sentence = _sentence;

        init = false;
        words.Clear();
        words = SplitIntoWords(_sentence);



        SpaunWords(words);

        SendStringToCheckStringInit(sentence);

        SendStringToCheckString();
        checkStr.Check();
        GetComponent<BrainHealthModifier>().SetStartValue();

        foreach(GameObject obj in hints)
        {
            obj.GetComponent<Renderer>().enabled = true;
        }

    }

    bool init = false;

    Dictionary<int, int> duplicateWordIndices = new Dictionary<int,int>();

    void InitialiseSentence()
    {




        FindWordPositions();

        foreach(GameObject word in wordObjects)
        {
            word.GetComponentInChildren<MeshRenderer>().enabled = true;
        }

        
    }


    void ShowHints()
    {
        for(int i = 0; i < wordObjects.Count && !complete; i++)
        {
            if(wordOrder[i] != i)
            {
                StartCoroutine(CR_ShowWordHint(wordObjects[i].transform));
                //do something to wordObject
            }
        }
    }

    private IEnumerator CR_ShowWordHint(Transform transform)
    {
        var t = 0f;
        while(t <= 0.5f)
        {
            if (t <= 0.25f)
            {
                transform.localScale = Vector3.Slerp(Vector3.one, Vector3.one * (1 + t), t * 4f);
            }
            else
            {
                transform.localScale = Vector3.Slerp(Vector3.one * (1 + t), Vector3.one, (t - 0.25f) * 4f);
            }

            t += Time.deltaTime;

            yield return endOfFrame;
        }
    }

    bool complete = false;



    bool CheckComplete()
    {
        bool noWordsGrabbed = true;

        for (int i = 0; i < wordObjects.Count; i++)
        {
            //print(wordObjects[i].GetComponent<GrabbedDetector>().isGrabbed.ToString());

            if (wordObjects[i].GetComponent<GrabbedDetector>().isGrabbed)//is grabbed
                noWordsGrabbed = false;
        }

        bool _complete = false;

        newSentence = "";
        for(int i = 0; i < words.Count; i++)
        {
            newSentence += words[wordOrder[i]];
            if (i < words.Count - 1)
                newSentence += " ";
        }
        if (newSentence == sentence && noWordsGrabbed)
            _complete = true;

        

        if (complete)
        {
            //print("COMPLETE");
        }
        else
        {
            //print("not complete");

        }

        return _complete;
    }

    public GameEvent levelComplete;

    void OnComplete()
    {
        complete = false;

        foreach (GameObject word in wordObjects)
            Destroy(word);
        wordObjects.Clear();
        levelComplete.Raise();
        sfx_complete.Raise();

        foreach (GameObject obj in hints)
        {
            obj.GetComponent<Renderer>().enabled = false;
        }

    }

    public CheckString checkStr;

    public void SendStringToCheckString()
    {

        newSentence = "";
        for (int i = 0; i < words.Count; i++)
        {
            newSentence += words[wordOrder[i]];
            if (i < words.Count - 1)
                newSentence += " ";
        }
        checkStr.Pieces = newSentence.Split(' ');
    }
    public void SendStringToCheckStringInit(string _sentence)
    {
        checkStr.Solution = _sentence;
    }


    Vector3 startScale = new Vector3();
    Vector3 endScale = new Vector3();
    float endTime = 0;

    void Update()
    {
        if(!init && wordObjects.Count > 0 && wordObjects[0].GetComponentInChildren<TMP_Text>().GetRenderedValues().x > 0)
        {
            InitialiseSentence();
            init = true;
        }

        if (init)
        {
            //if (hintTime >= 5f)
            //{
            //    ShowHints();
            //    hintTime = 0f;
            //}
            //else
            //    hintTime += Time.deltaTime;

            MoveWords();

            CheckWordOrder();



            if (!complete)
            {
                if ((wordObjects.Count > 0 && CheckComplete() )|| Input.GetKeyDown(KeyCode.KeypadEnter) )
                {
                    complete = true;
                    DoCompleteRoutine();
                    if(wordObjects.Count != 0)
                        startScale = wordObjects[0].transform.localScale;
                    print(startScale);
                    endScale = startScale * 10f;
                    endTime = Time.time;
                    print("COMPLETE");
                }
            }

            //complete means words get bigger and fade out
            if (complete)
            {
                foreach (GameObject word in wordObjects) {
                    float tt = (Time.time - endTime) / completeRoutineDuration;
                    word.transform.localScale = Vector3.Lerp(startScale, endScale, tt);
                    word.GetComponentInChildren<TMP_Text>().color = Color.Lerp(completeColor, completeColor2, tt);
                }
            }

        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            SetUpNewSentence(example);
        }

    }

    public Color completeColor = Color.green;
    public Color completeColor2 = Color.magenta;


    void DoCompleteRoutine()
    {
        foreach(GameObject word in wordObjects)
        {
            word.GetComponentInChildren<TMP_Text>().color = completeColor;
        }

        Invoke("OnComplete", completeRoutineDuration);
    }


    public static void ShuffleArray(List<int> arr)
    {
        for (int i = arr.Count - 1; i > 0; i--)
        {
            int r = UnityEngine.Random.Range(0, i);
            int tmp = arr[i];
            arr[i] = arr[r];
            arr[r] = tmp;
        }
    }
}
