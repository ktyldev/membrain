﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class JaroWinklerDistance
{
    /* The Winkler modification will not be applied unless the 
     * percent match was at or above the mWeightThreshold percent 
     * without the modification. 
     * Winkler's paper used a default value of 0.7
     */
    private static readonly float mWeightThreshold = 0.7f;

    /* Size of the prefix to be concidered by the Winkler modification. 
     * Winkler's paper used a default value of 4
     */
    private static readonly int mNumChars = 4;

    public static float Distance(string aString1, string aString2)
    {
        return Proximity(aString1, aString2);
    }

    private static float Proximity(string aString1, string aString2)
    {
        int lLen1 = aString1.Length;
        int lLen2 = aString2.Length;
        if (lLen1 == 0)
            return lLen2 == 0 ? 1.0f : 0.0f;

        int lSearchRange = Mathf.Max(0, Mathf.Max(lLen1, lLen2) / 2 - 1);

        // default initialized to false
        bool[] lMatched1 = new bool[lLen1];
        bool[] lMatched2 = new bool[lLen2];

        int lNumCommon = 0;
        for (int i = 0; i < lLen1; ++i)
        {
            int lStart = Mathf.Max(0, i - lSearchRange);
            int lEnd = Mathf.Min(i + lSearchRange + 1, lLen2);
            for (int j = lStart; j < lEnd; ++j)
            {
                if (lMatched2[j]) continue;
                if (aString1[i] != aString2[j])
                    continue;
                lMatched1[i] = true;
                lMatched2[j] = true;
                ++lNumCommon;
                break;
            }
        }
        if (lNumCommon == 0) return 0.0f;

        int lNumHalfTransposed = 0;
        int k = 0;
        for (int i = 0; i < lLen1; ++i)
        {
            if (!lMatched1[i]) continue;
            while (!lMatched2[k]) ++k;
            if (aString1[i] != aString2[k])
                ++lNumHalfTransposed;
            ++k;
        }
        // System.Diagnostics.Debug.WriteLine("numHalfTransposed=" + numHalfTransposed);
        int lNumTransposed = lNumHalfTransposed / 2;

        // System.Diagnostics.Debug.WriteLine("numCommon=" + numCommon + " numTransposed=" + numTransposed);
        float lNumCommonD = lNumCommon;
        float lWeight = (lNumCommonD / lLen1
                         + lNumCommonD / lLen2
                         + (lNumCommon - lNumTransposed) / lNumCommonD) / 3.0f;

        if (lWeight <= mWeightThreshold) return lWeight;
        int lMax = Mathf.Min(mNumChars, Mathf.Min(aString1.Length, aString2.Length));
        int lPos = 0;
        while (lPos < lMax && aString1[lPos] == aString2[lPos])
            ++lPos;
        if (lPos == 0) return lWeight;
        return lWeight + 0.1f * lPos * (1.0f - lWeight);

    }


}
