﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public BrainHealth health;

    public static LevelManager Instance { get; private set; }

    private HintLoader hintLoader;
    private int _completedLevels = 0;
    private const int _totalLevels = 5;

    public int CurrentLevel { get; private set; } = 0;
    public bool CompletedAllLevels => _completedLevels == _totalLevels;

    public SentenceControl sentenceControl;

    private void Awake()
    {
        health.Health = 0;

        if (Instance != null)
        {
            Debug.LogError("BAD");
            return;
        }

        Instance = this;

        hintLoader = new HintLoader();
    }

    public void LoadLevel(int i)
    {
        CurrentLevel = i -1;

        sentenceControl.SetUpNewSentence(hintLoader.Hints[i-1].English);
    }

    public void LevelCompleted()
    {
        _completedLevels++;
        health.Health = Mathf.Lerp(0, 1, _completedLevels / (float)_totalLevels);
    }
}
