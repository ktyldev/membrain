﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSetting : MonoBehaviour
{
    public GameObject StartCamera;
    public GameObject InitialTrack;
    public CinemachineFreeLook FreeLookCamera;

    public void SetCamerasForGameStart()
    {
        FreeLookCamera.gameObject.SetActive(true);
        StartCamera.SetActive(false);
        InitialTrack.SetActive(false);

        StartCoroutine(CR_Wait());
    }

    private IEnumerator CR_Wait()
    {
        yield return new WaitForSeconds(0.125f);
        FreeLookCamera.m_Transitions.m_InheritPosition = false;
    }
}
