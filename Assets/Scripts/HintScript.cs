﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using TMPro;
using System.IO;

public class HintScript : MonoBehaviour
{
    private HintLoader hintLoader;

    private void Start()
    {
        hintLoader = new HintLoader();
    }

    // Start is called before the first frame update
    public void LoadLevelHints()
    {
        

        // Label the hints from the CSV header.
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            TMP_Text text = child.GetComponent<TMP_Text>();
            text.text = hintLoader.Hints[LevelManager.Instance.CurrentLevel].hints[i+1];
        }
    }

   

}
