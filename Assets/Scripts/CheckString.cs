﻿using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(CheckString))]
public class CheckStringEditor : Editor
{
    private CheckString _data;

    private void OnEnable()
    {
        _data = target as CheckString;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Shuffle"))
        {
            _data.Generate();
        }
    }
}
#endif

public class CheckString : MonoBehaviour
{
    public string Solution = "The quick brown fox jumped over the lazy dog";
    public float MinimumEquality = 0.5f;
    public float MaximumEquality = 0.75f;
    [HideInInspector]
    public string[] Pieces;

    public UnityEvent OnCheck;

    private float sentenceEquality = 0f;
    [HideInInspector]
    public float SentenceEquality
    {
        set
        {
            sentenceEquality = value;
            OnCheck.Invoke();
        }

        get
        {
            return sentenceEquality;
        }
    }

    public string CurrentString => string.Join(" ", Pieces);

    public void Generate()
    {
        Pieces = Solution.Split(' ');
        SentenceEquality = 0f;
        while (SentenceEquality < MinimumEquality || SentenceEquality > MaximumEquality)
        {
            Shuffle();
            Check();
        }
    }

    private void Shuffle()
    {
        Pieces.Shuffle();

#if UNITY_EDITOR
        Debug.Log(CurrentString);
#endif
    }

    public void Check()
    {
        SentenceEquality = JaroWinklerDistance.Distance(Solution, CurrentString);
#if UNITY_EDITOR
        Debug.Log(SentenceEquality);
#endif
    }
}
