﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
public class Neuron : MonoBehaviour
{
    public int ConnectedNeurons => connectedNeurons.Count;

    private List<GameObject> synapses = new List<GameObject>();
    private List<GameObject> connectedNeurons = new List<GameObject>();

    public void BuildConnections(int maxConnections, float maxRadius, GameObject synapsePrefab, GameObject emptyPrefab)
    {
        if (ConnectedNeurons >= maxConnections)
            return;

        var colliders = Physics.OverlapSphere(transform.position, maxRadius);
        if (colliders.Length > maxConnections)
            colliders.Shuffle();

        foreach(var collider in colliders)
        {
            var neuron = collider.gameObject.GetComponent<Neuron>();

            if (neuron == null || neuron.gameObject == gameObject || neuron.IsConnectedTo(this.gameObject) || neuron.ConnectedNeurons >= maxConnections)
                continue;

            AddConnection(neuron.gameObject, synapsePrefab);
            neuron.AddReference(gameObject);

            if (ConnectedNeurons >= maxConnections)
                return;
        }
    }

    public void AddConnection(GameObject obj, GameObject prefab)
    {
        var offsettedPosition = Vector3.Normalize(obj.transform.position - transform.position) * 3f + gameObject.transform.position;
        float yScale = Vector3.Distance(transform.position, obj.transform.position) - 6f;

        var synapse = Instantiate(prefab,
            offsettedPosition,
            Quaternion.identity,
            transform);

        synapse.transform.LookAt(obj.transform);
        synapse.transform.localScale = new Vector3(0.5f, 0.5f, yScale);

        synapses.Add(synapse);

        AddReference(obj);
    }

    public void AddReference(GameObject obj)
    {
        connectedNeurons.Add(obj);
    }

    public bool IsConnectedTo(GameObject obj)
    {
        return connectedNeurons.Contains(obj);
    }

    public void DestroyChildren()
    {
        foreach (var s in synapses)
        {
#if UNITY_EDITOR
            DestroyImmediate(s);
#else
            Destroy(s);
#endif
        }
        synapses = new List<GameObject>();
    }
}
