﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

public class HintLoader
{
    public List<Hint> Hints { get; private set; }

    public HintLoader()
    {
        Hints = new List<Hint>();
        var lines = File.ReadAllLines(Application.dataPath + "/story.csv");
        foreach(var line in lines)
        {
            Hints.Add(new Hint(line));
        }
    }
}

public class Hint
{
    public string[] hints = new string[6];

    public Hint(string line)
    {
        hints = line.Split('\t');
    }

    public string English => hints[0];
    public string French => hints[1];
    public string Italian => hints[2];
    public string Spanish => hints[3];
    public string German => hints[4];
    public string Russian => hints[5];
}