﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(NeuronMapShaderControl))]
public class NeuronMapShaderControlEditor : Editor
{
    private NeuronMapShaderControl _data;

    private void OnEnable()
    {
        _data = target as NeuronMapShaderControl;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Fade Out"))
        {
            _data.FadeOut(); 
        }

        if (GUILayout.Button("Fade In"))
        {
            _data.FadeIn();
        }
    }
}
#endif

public class NeuronMapShaderControl : MonoBehaviour
{
    public float fadeTime;
    public UnityEvent onFadeOut;
    public UnityEvent onFadeIn;

    private Renderer[] _renderers;
    private WaitForEndOfFrame _wait = new WaitForEndOfFrame();

    private void OnEnable()
    {
        _renderers = GetComponentsInChildren<Renderer>();
    }

    public void FadeOut()
    {
        StartCoroutine(Fade(1));
    }

    public void FadeIn()
    {
        StartCoroutine(Fade(-1));
    }

    private IEnumerator Fade(int dir)
    {
        if (Mathf.Abs(dir) != 1)
        {
            throw new System.Exception("fuck u c:");
        }

        float start = Time.time;
        float elapsed = Time.time - start;

        MaterialPropertyBlock props = new MaterialPropertyBlock();

        while (elapsed < fadeTime)
        {
            elapsed += Time.deltaTime;
            float t = elapsed / fadeTime;

            t = dir == 1 ? t : 1 - t;

            //float dissolve;

            foreach (Renderer renderer in _renderers)
            {
                //props.SetColor("_Main", Color.Lerp(Color.white, Color.clear, t));
                //props.SetColor("_Edge", Color.Lerp(Color.white, Color.clear, t));
                props.SetFloat("_Dissolve", Mathf.Lerp(0, 1, t));

                renderer.SetPropertyBlock(props);
            }
             
            yield return _wait;
        }

        if (dir == 1) // fade out
        {
            onFadeOut.Invoke();
        }
        else
        {
            onFadeIn.Invoke();
        }
    }
}
