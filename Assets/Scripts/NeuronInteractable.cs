﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Interactable))]
public class NeuronInteractable : MonoBehaviour
{
    public GameEvent loadLevel;
    public GameEvent hover;

    private Vector3 _defaultPosition;
    // probably don't have to worry about rotation for our unlit shader :)

    private Interactable _interactable;
    private Hand.AttachmentFlags _attachmentFlags = Hand.defaultAttachmentFlags 
        & ( ~Hand.AttachmentFlags.SnapOnAttach ) 
        & (~Hand.AttachmentFlags.DetachOthers) 
        & (~Hand.AttachmentFlags.VelocityMovement);

    private Renderer _inner;

    private void Awake()
    {
        _interactable = GetComponent<Interactable>();
        //_inner = transform.Find("Inner").GetComponent<Renderer>();
        //_inner.material.SetColor("_Main", Color.white);
    }

    private void OnHandHoverBegin(Hand hand)
    {
        hover.Raise();
    }

    private void HandHoverUpdate(Hand hand)
    {
        GrabTypes startingGrabType = hand.GetGrabStarting();
        bool isGrabEnding = hand.IsGrabEnding(gameObject);

        if (_interactable.attachedToHand == null && startingGrabType != GrabTypes.None)
        {
            _defaultPosition = transform.position;

            loadLevel.Raise();

            //hand.HoverLock(_interactable);
            //hand.AttachObject(gameObject, startingGrabType, _attachmentFlags);
        }
        else if (isGrabEnding)
        {
            //hand.DetachObject(gameObject);
            //hand.HoverUnlock(_interactable);

            //// TODO: nice lerp
            //transform.position = _defaultPosition;
        }
    }
}
