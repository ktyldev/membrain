﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource mainSource;
    public AudioSource effectSource;

    [Range(0f, 5f)]
    public float fadingTime = 1f;

    public AudioClip endingClip;

    public void PlaySoundEffect(AudioClip effectClip)
    {
        if (effectSource.isPlaying)
            effectSource.Stop();

        effectSource.clip = effectClip;
        effectSource.Play();
    }

    public void PlayAmbienceEffect(AudioClip ambienceClip)
    {
        StartCoroutine(IVolumeFader(mainSource, LevelManager.Instance.CompletedAllLevels ? endingClip : ambienceClip));
    }

    private IEnumerator IVolumeFader(AudioSource source, AudioClip clip)
    {
        float baseVolume = source.volume;
        float t = 0;

        while (t < fadingTime)
        {
            source.volume = Mathf.Lerp(baseVolume, 0, t);
            t += Time.deltaTime;
            yield return null;
        }
        
        source.Stop();

        t = 0;

        source.clip = clip;
        source.Play();

        while (t < fadingTime)
        {
            source.volume = Mathf.Lerp(0, baseVolume, t);
            t += Time.deltaTime;
            yield return null;
        }

        source.volume = baseVolume;
    }
}
