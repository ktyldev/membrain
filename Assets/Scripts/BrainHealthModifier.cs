﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CheckString))]
public class BrainHealthModifier : MonoBehaviour
{
    public BrainHealth brain;

    private CheckString checker;

    private float startValue = 0f;

    private void Awake()
    {
        checker = GetComponent<CheckString>();
    }

    public void ModifyHealthBrain()
    {
        if (startValue != 0)
            StartCoroutine(CR_SetBrainHealth());
            //brain.Health = Mathf.Clamp01((checker.SentenceEquality - startValue) / (1 - startValue));
    }

    private IEnumerator CR_SetBrainHealth()
    {
        float t = 0f;
        while (t <= 0.25f)
        {
            brain.Health = Mathf.Lerp(brain.Health,
                Mathf.Clamp01((checker.SentenceEquality - startValue) / (1 - startValue)),
                t * 4f);    //yay, magic numbers!

            t += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }
    }

    public void SetStartValue()
    {
        startValue = checker.SentenceEquality;
        ModifyHealthBrain();
    }
}
