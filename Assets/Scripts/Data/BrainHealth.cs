﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BrainHealth : ScriptableObject
{
    [Range(0, 1)]
    [SerializeField]
    private float _health;
    public float Health 
    { 
        set
        {
            _health = value;
            Update();
        }
        get => _health;
    }

    [Header("Materials")]
    public Material synapse;
    public Material background;
    public Material neuronInner;
    public Material neuronOuter;

    [System.Serializable]
    public struct Synapse
    {
        public float waist;
        public Color main;
        public Color edge;
    }
    [System.Serializable]
    public struct Background
    {
        public Color colour;
    }
    [System.Serializable]
    public struct Neuron
    {
        public Color innerMain;
        public Color innerEdge;
        public Color outerMain;
        public Color outerEdge;
    }

    [System.Serializable]
    public struct Parameters
    {
        public Neuron neuron;
        public Synapse synapse;
        public Background background;
    }
    public Parameters sick;
    public Parameters healthy;

    private Color _currentBackgroundColor;
    public Color CurrentBackgroundColour 
    {
        get => _currentBackgroundColor;
        set
        {
            RenderSettings.fogColor = value;
            _currentBackgroundColor = value;
        }
    }


    private void OnValidate()
    {
        Update();
    }

    public void Update()
    {
        UpdateSynapses();
        UpdateBackground();
        UpdateNeurons();
    }

    private void UpdateNeurons()
    {
        Color innerMain = Color.Lerp(sick.neuron.innerMain, healthy.neuron.innerMain, _health);
        neuronInner.SetColor("_Main", innerMain);
        Color innerEdge = Color.Lerp(sick.neuron.innerEdge, healthy.neuron.innerEdge, _health);
        neuronInner.SetColor("_Edge", innerEdge);

        Color outerMain = Color.Lerp(sick.neuron.outerMain, healthy.neuron.outerMain, _health);
        neuronOuter.SetColor("_Main", outerMain);
        Color outerEdge = Color.Lerp(sick.neuron.outerEdge, healthy.neuron.outerEdge, _health);
        neuronOuter.SetColor("_Edge", outerEdge);
    }

    private void UpdateSynapses()
    {
        float waist = Mathf.Lerp(sick.synapse.waist, healthy.synapse.waist, _health);
        synapse.SetFloat("_Waist", waist);

        Color main = Color.Lerp(sick.synapse.main, healthy.synapse.main, _health);
        synapse.SetColor("_Main", main);

        Color edge = Color.Lerp(sick.synapse.edge, healthy.synapse.edge, _health);
        synapse.SetColor("_Edge", edge);
    }

    private void UpdateBackground()
    {
        CurrentBackgroundColour = Color.Lerp(sick.background.colour, healthy.background.colour, _health);
        //background.SetColor("_Tint", colour);
        //RenderSettings.fogColor = colour;
    }
}
