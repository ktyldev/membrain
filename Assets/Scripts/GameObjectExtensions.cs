﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions
{
    public static void DestroyDeep(this GameObject obj, bool runInEditor)
    {
        if (obj == null)
            return;

        while (obj.transform.childCount > 0)
        {
            obj.transform.GetChild(0).gameObject.DestroyDeep(runInEditor);
        }
        
        if(runInEditor)
        {
            GameObject.DestroyImmediate(obj);
        }
        else
        {
            GameObject.Destroy(obj);
        }
    }
}
