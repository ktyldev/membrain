﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(NeuronMapGenerator))]
public class NeuronMapGeneratorEditor : Editor
{
    private NeuronMapGenerator _data;

    private void OnEnable()
    {
        _data = target as NeuronMapGenerator;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Generate"))
        {
            _data.Clear();
            _data.Generate();
        }

        if (GUILayout.Button("Clear"))
        {
            _data.Clear();
        }
    }
}
#endif

public class NeuronMapGenerator : MonoBehaviour
{
    public GameObject SynapsePrefab;
    public GameObject NeuronPrefab;
    public GameObject EmptyPrefab;
    public float MaxRadius = 100;
    public float InnerRadius = 20f;
    public int NeuronsCount = 100;
    public float NeuronSynapseCatch = 50f;
    public int MaxSynapsesPerNeuron = 6;
    public float postScale = 1f;

    [SerializeField]
    private bool ShowGizmos = true;
    public bool stripColliders = true;
    public bool combineMeshes = true;

    private GameObject[] neurons;

    public void Clear()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            GameObject go = transform.GetChild(i).gameObject;
#if UNITY_EDITOR
            DestroyImmediate(go);
#else
            Destroy(go);
#endif
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (ShowGizmos)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, InnerRadius * postScale);
            Gizmos.DrawWireSphere(transform.position, MaxRadius * postScale);
            //Gizmos.color = new Color(0f, 1f, 0f, 0.25f);
            //Gizmos.DrawSphere(transform.position, MaxRadius);
            //Gizmos.color = new Color(1f, 0f, 0f, 0.25f);
            //Gizmos.DrawSphere(transform.position, InnerRadius);
        }        
    }

    public void Generate()
    {
        transform.localScale = Vector3.one;

        neurons = new GameObject[NeuronsCount];
        var neuronsScripts = new Neuron[NeuronsCount];

        //generating the neurons
        for (int i = 0; i < NeuronsCount; i++)
        {
            Vector3 pos = Vector3.zero;
            while(pos.IsInsideSphere(InnerRadius))
            {
                pos = Random.insideUnitSphere * MaxRadius;
            }
            neurons[i] = Instantiate(NeuronPrefab, pos, Random.rotation, transform);
            neuronsScripts[i] = neurons[i].GetComponent<Neuron>();
        }

        foreach(var n in neurons)
        {
            var ns = n.GetComponent<Neuron>();
        }

        //generating connections
        foreach(var n in neurons)
        {
            var ns = n.GetComponent<Neuron>();
            ns.BuildConnections(MaxSynapsesPerNeuron, NeuronSynapseCatch, SynapsePrefab, EmptyPrefab);            
        }

        foreach(var n in neuronsScripts.Where(x => x.ConnectedNeurons == 0))
        {
            n.DestroyChildren();
#if UNITY_EDITOR
            DestroyImmediate(n.gameObject);
#else
            Destroy(n.gameObject);
#endif
        }

        transform.localScale *= postScale;

        var nmsc = GetComponent<NeuronMapShaderControl>();
        if (nmsc != null)
        {
            nmsc.enabled = true;
        }
        if (stripColliders)
        {
            List<Collider> colliders = new List<Collider>();
            foreach (var n in neurons)
            {
                if (n == null)
                {
                    continue;
                }

                colliders.Add(n.GetComponent<Collider>());
                colliders.AddRange(n.GetComponentsInChildren<Collider>());
            }
            for (int i = 0; i < colliders.Count; i++)
            {
                if (colliders[i] != null)
                {
#if UNITY_EDITOR
            DestroyImmediate(colliders[i]);
#else
            Destroy(colliders[i]);
#endif
                }
            }
        }
    }

    private void CombineMeshes()
    {
        MeshFilter filter = gameObject.GetComponent<MeshFilter>();

        Mesh[] meshes = GetComponentsInChildren<MeshFilter>().Select(f => f.mesh).ToArray();
        Debug.Log($"Got {meshes.Length} meshes");
        
        var combine = new CombineInstance[meshes.Length];
        for (int i = 0; i < meshes.Length; i++)
        {
            combine[i].mesh = meshes[i];
            combine[i].transform = transform.localToWorldMatrix;
        }

        var mesh = new Mesh();
        mesh.CombineMeshes(combine);
        filter.mesh = mesh;
    }

    private void Start()
    {
        if (combineMeshes)
        {
            CombineMeshes();

            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
    }
}
