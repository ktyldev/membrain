﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BackgroundHeartbeat : MonoBehaviour
{
    public BrainHealth BrainHealth;
    public AnimationCurve heartBeat;
    [Range(40, 200)]
    public int BPM = 80;
    //public Color Back = Color.black;
    //public Color Front = Color.red;
    public Material backgroundMaterial;
    public Color colour;

    private float time;

    private float BeatFrequency => 60f / BPM;
    private float beatLengthInSeconds;

    private void Start()
    {
        
        beatLengthInSeconds = heartBeat.keys.Last().time;
        time = Time.time;
    }

    private void Update()
    {
        if (Time.time > time + BeatFrequency)
        {
            time = Time.time;
        }
        float timeDiff = Time.time - time;
        var color = BrainHealth.CurrentBackgroundColour;
        if (timeDiff <= beatLengthInSeconds)
        {
            color = Color.Lerp(
                color, 
                colour, 
                Mathf.Lerp(0, heartBeat.Evaluate(timeDiff), 1 - BrainHealth.Health));
        }
        backgroundMaterial.SetColor("_Tint", color);
        RenderSettings.fogColor = color;
    }
}
