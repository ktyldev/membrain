﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelSelect : MonoBehaviour
{
    public BrainHealth brainHealth;
    public LevelManager levelManager;
    public GameObject[] levelNeurons;

    private Neuron[] _neurons;
    private Neuron? _currentHighlighted;

    private struct Neuron
    {
        public Renderer inner;
        public Renderer outer;
    }

    private void OnEnable()
    {
        _neurons = levelNeurons.Select(n =>
        {
            var outer = n.transform
                .Find("Outer")
                .GetComponent<Renderer>();
            var inner = n.transform
                .Find("Inner")
                .GetComponent<Renderer>();

            return new Neuron
            {
                inner = inner,
                outer = outer
            };

        }).ToArray();

        _currentHighlighted = _neurons[0];
    }

    private void LateUpdate()
    {
        if (!_currentHighlighted.HasValue)
        {
            return;
        }

        foreach (Neuron n in _neurons)
        {
            n.outer.material.SetFloat("_PulseAmount", 0);
            n.inner.material.SetFloat("_PulseAmount", 0);
        }

        for (int i = 0; i < levelManager.CurrentLevel; i++)
        {
            var n = _neurons[i];
            n.inner.material.SetColor("_Main", brainHealth.healthy.neuron.innerMain);
            n.inner.material.SetColor("_Edge", brainHealth.healthy.neuron.innerEdge);
            n.outer.material.SetColor("_Main", brainHealth.healthy.neuron.outerMain);
            n.outer.material.SetColor("_Edge", brainHealth.healthy.neuron.outerEdge);
        }

        float amount = Mathf.Clamp(Mathf.Sin(Time.time * 2), 0, 1);
        _currentHighlighted.Value.outer.material.SetFloat("_PulseAmount", amount);

        for (int i = levelManager.CurrentLevel + 1; i < 5; i++)
        {
            var n = _neurons[i];
            n.inner.material.SetColor("_Main", brainHealth.sick.neuron.innerMain);
            n.inner.material.SetColor("_Edge", brainHealth.sick.neuron.innerEdge);
            n.outer.material.SetColor("_Main", brainHealth.sick.neuron.outerMain);
            n.outer.material.SetColor("_Edge", brainHealth.sick.neuron.outerEdge);
        }
    }

    public void OnLevelCompleted()
    {
        if (levelManager.CurrentLevel >= 5)
            return;

        _currentHighlighted = _neurons[levelManager.CurrentLevel];
    }
}
