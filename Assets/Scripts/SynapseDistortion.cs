﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SynapseDistortion : MonoBehaviour
{
    public Renderer[] renderers;


    private void OnEnable()
    {
        MaterialPropertyBlock props = new MaterialPropertyBlock();
        foreach (Renderer renderer in renderers)
        {
            props.SetVector("_XCoords", GetCoords());
            props.SetVector("_YCoords", GetCoords());

            renderer.SetPropertyBlock(props);
        }     
    }

    private Vector4 GetCoords()
    {
        var p1 = GetCoordPair();
        var p2 = GetCoordPair();

        return new Vector4(p1.x, p1.y, p2.x, p2.y);
    }

    private Vector2 GetCoordPair()
    {
        return new Vector2
        {
            x = UnityEngine.Random.Range(0f, 1f),
            y = UnityEngine.Random.Range(0f, 1f),
        };
    }
}
