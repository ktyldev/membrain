﻿using UnityEngine;
using System.Collections;

public static class Utils
{
    public static void Shuffle<T>(this T[] array)
    {
        var currentIndex = array.Length;
        T temporaryValue;
        int randomIndex;

        // While there remain elements to shuffle...
        while (0 != currentIndex)
        {
            // Pick a remaining element...
            randomIndex = (int)Mathf.Floor(Random.value * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
    }

    public static bool IsInsideSphere(this Vector3 v, float radius)
    {
        return IsInsideSphere(v, radius, Vector3.zero);
    }

    public static bool IsInsideSphere(this Vector3 v, float radius, Vector3 center)
    {
        return Vector3.Distance(v, center) <= radius;
    }

}
