﻿Shader "Custom/neuron_surf"
{
    Properties
    {
        _Main ("Main", Color) = (1,1,1,1)
        _Edge ("Edge", Color) = (1,1,1,1)
        _Pulse("Pulse Colour", Color) = (1,1,1,1)
        _PulseAmount("Pulse Amount", Range(0, 1)) = 0.0

        _Fresnel("Fresnel", Range(0, 1)) = 1.0

        _Dissolve("Dissolve", Range(0, 1)) = 0.0
        _Noise("Noise", 2D) = "black" {}

        [HideInInspector] _MainTex ("Albedo (RGB)", 2D) = "white" {}
    }
    SubShader
    {
        Tags { 
			"RenderType"="Transparent" 
			"Queue"="Transparent" 
		}
        LOD 200

		Blend SrcAlpha OneMinusSrcAlpha
		Zwrite Off

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Lambert alpha

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            UNITY_FOG_COORDS(1)
			float2 uv_MainTex;
			float4 vertex : SV_POSITION;
            float3 worldPos : TEXCOORD0;
            float3 worldNormal : TEXCOORD1;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        fixed4 _Pulse;

		float _Fresnel;
        sampler2D _Noise;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            UNITY_DEFINE_INSTANCED_PROP(fixed4, _Main)
            UNITY_DEFINE_INSTANCED_PROP(fixed4, _Edge)
            UNITY_DEFINE_INSTANCED_PROP(float, _Dissolve)
            UNITY_DEFINE_INSTANCED_PROP(float, _PulseAmount)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutput o)
        {
            // dissolve
            half dissolve = tex2D(_Noise, IN.uv_MainTex).r;
            clip(dissolve - UNITY_ACCESS_INSTANCED_PROP(Props, _Dissolve));
            
            // fresnel
            float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - IN.worldPos.xyz);
            half rim = 1.0 - saturate(dot(viewDir, IN.worldNormal));

            rim = lerp(1.0, rim, _Fresnel);

            fixed4 main = UNITY_ACCESS_INSTANCED_PROP(Props, _Main);
            fixed4 edge = UNITY_ACCESS_INSTANCED_PROP(Props, _Edge);

            edge = lerp(edge, _Pulse, UNITY_ACCESS_INSTANCED_PROP(Props, _PulseAmount));

			fixed4 col = lerp(main, edge, rim);

            o.Albedo = col.rgb;
            o.Alpha = col.a;

            UNITY_APPLY_FOG(IN.fogCoord, col);
        }
        ENDCG
    }
    FallBack "Diffuse"
}
