﻿Shader "Unlit/synapse"
{
    Properties
    {
        _Noise("Noise", 2D) = "black" {}
        // these need to be instanced
        _XCoords("X Coords", Vector) = (0,0,0,0)
        _YCoords("Y Coords", Vector) = (0,0,0,0)

        _Main("Main", Color) = (1,1,1,1)
        _Edge("Edge", Color) = (1,1,1,1)
        _Fresnel("Fresnel", Range(0, 1)) = 1.0

        _Waist("Waist", Range(0, 1)) = 1.0
    }
    SubShader
    {
        Tags 
		{ 
        "RenderType"="Transparent" 
        "Queue" = "Transparent"
		}
        LOD 100

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            Zwrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 normal : NORMAL;
            };

            struct v2f
            {
                //float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 worldPos : TEXCOORD0;
                float3 worldNormal : TEXCOORD1;
            };

            sampler2D _Noise;

            fixed4 _Main;
            fixed4 _Edge;
            float _Fresnel;

            float _Waist;

            UNITY_INSTANCING_BUFFER_START(Props)
            UNITY_DEFINE_INSTANCED_PROP(float4, _XCoords);
            UNITY_DEFINE_INSTANCED_PROP(float4, _YCoords);
            UNITY_INSTANCING_BUFFER_END(Props)

            v2f vert (appdata v)
            {
                v2f o;

                // calculate waist
                float d = v.vertex.z;
                float w = (1.0 - _Waist) * pow(2 * d, 2) + _Waist;
                v.vertex.x *= w;
                v.vertex.y *= w;

                // distort on X and Y
                float4 xco = UNITY_ACCESS_INSTANCED_PROP(Props, _XCoords);
                float4 yco = UNITY_ACCESS_INSTANCED_PROP(Props, _YCoords);
                float2 xuv = lerp(xco.xy, xco.zw, d + 0.5);
                float2 yuv = lerp(yco.xy, yco.zw, d + 0.5);
                fixed4 xcol = tex2Dlod(_Noise, float4(xuv,0,0));
                fixed4 ycol = tex2Dlod(_Noise, float4(yuv,0,0));
                v.vertex.x += xcol.r - 0.5;
                v.vertex.y += ycol.r - 0.5;

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xyz);
                half rim = 1.0 - saturate(dot(viewDir, i.worldNormal));

                rim = lerp(1.0, rim, _Fresnel);

                fixed4 col = lerp(_Main, _Edge, rim);

                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
