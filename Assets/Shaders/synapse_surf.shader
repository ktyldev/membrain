﻿Shader "Custom/synapse_surf"
{
    Properties
    {
        _Noise("Noise", 2D) = "black" {}
        // these need to be instanced
        _XCoords("X Coords", Vector) = (0,0,0,0)
        _YCoords("Y Coords", Vector) = (0,0,0,0)

        _Main("Main", Color) = (1,1,1,1)
        _Edge("Edge", Color) = (1,1,1,1)
        _Fresnel("Fresnel", Range(0, 1)) = 1.0

        _DissolveNoise("Dissolve Noise", 2D) = "black" {}
        _Dissolve("Dissolve", Range(0, 1)) = 0.0

        _Waist("Waist", Range(0, 1)) = 1.0
    }
    SubShader
    {
        Tags 
		{ 
			"RenderType"="Transparent" 
			"Queue" = "Transparent"
		}
        LOD 200

		Blend SrcAlpha OneMinusSrcAlpha
		Zwrite Off

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Lambert alpha //fullforwardshadows
		#pragma vertex vert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

		#include "UnityCG.cginc"

        sampler2D _MainTex;

        struct Input
        {
            UNITY_FOG_COORDS(1)
			float2 uv_MainTex;
			float4 vertex : SV_POSITION;
			float3 worldPos : TEXCOORD0;
			float3 worldNormal : TEXCOORD1;
            float3 modelPos : TEXCOORD2;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        float _Waist;
		float _Fresnel;
		sampler2D _Noise;
		//fixed4 _Main;
		//fixed4 _Edge;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)

            UNITY_DEFINE_INSTANCED_PROP(fixed4, _Main)
            UNITY_DEFINE_INSTANCED_PROP(fixed4, _Edge)
            
            UNITY_DEFINE_INSTANCED_PROP(float4, _XCoords)
            UNITY_DEFINE_INSTANCED_PROP(float4, _YCoords)

            UNITY_DEFINE_INSTANCED_PROP(float, _Dissolve)

        UNITY_INSTANCING_BUFFER_END(Props)

		void vert (inout appdata_full v, out Input o)
		{
            UNITY_INITIALIZE_OUTPUT(Input, o);

			// calculate waist
			float d = v.vertex.z;
			float w = (1.0 - _Waist) * pow(2 * d, 2) + _Waist;
			v.vertex.x *= w;
			v.vertex.y *= w;

			// distort on X and Y
			float4 xco = UNITY_ACCESS_INSTANCED_PROP(Props, _XCoords);
			float4 yco = UNITY_ACCESS_INSTANCED_PROP(Props, _YCoords);
			float2 xuv = lerp(xco.xy, xco.zw, d + 0.5);
			float2 yuv = lerp(yco.xy, yco.zw, d + 0.5);
			fixed4 xcol = tex2Dlod(_Noise, float4(xuv,0,0));
			fixed4 ycol = tex2Dlod(_Noise, float4(yuv,0,0));
			v.vertex.x += xcol.r - 0.5;
			v.vertex.y += ycol.r - 0.5;

            o.modelPos = v.vertex;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.worldNormal = UnityObjectToWorldNormal(v.normal);
			o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

			UNITY_TRANSFER_FOG(o,o.vertex);
			//return o;
		}

        void surf (Input IN, inout SurfaceOutput o)
        {
            // dissolve
            float2 p = IN.modelPos + float2(0.5, 0.5); // ¯\_(ツ)_/¯

            half dissolve = tex2D(_Noise, p.xy).x;
            clip(dissolve - UNITY_ACCESS_INSTANCED_PROP(Props, _Dissolve));

            // fresnel
			float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - IN.worldPos.xyz);
			half rim = 1.0 - saturate(dot(viewDir, IN.worldNormal));

			rim = lerp(1.0, rim, _Fresnel);

            fixed4 main = UNITY_ACCESS_INSTANCED_PROP(Props, _Main);
            fixed4 edge = UNITY_ACCESS_INSTANCED_PROP(Props, _Edge);

			fixed4 col = lerp(main, edge, rim);

            o.Albedo = col.rgb;
            o.Alpha = col.a;

			UNITY_APPLY_FOG(IN.fogCoord, col);

            //o.Metallic =    0.0;
            //o.Smoothness =  0.0;

            //// Albedo comes from a texture tinted by color
            //fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            //o.Albedo = c.rgb;
            //// Metallic and smoothness come from slider variables
            //o.Metallic = _Metallic;
            //o.Smoothness = _Glossiness;
            //o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
