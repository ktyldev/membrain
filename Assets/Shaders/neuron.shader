﻿Shader "Unlit/neuron"
{
    Properties
    {
        _Main ("Main", Color) = (1,1,1,1)
        _Edge ("Edge", Color) = (1,1,1,1)
        _Fresnel("Fresnel", Range(0, 1)) = 1.0

        [HideInInspector]_MainTex("Albedo", 2D) = "white" {}
    }
    SubShader
    {
        Tags 
		{ 
			"RenderType"="Transparent" 
            "Queue"="Transparent"
		}
        LOD 100

        Pass
        {
		    Blend SrcAlpha OneMinusSrcAlpha 
            Zwrite Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 worldPos : TEXCOORD2;
                float3 worldNormal : TEXCOORD3;
            };

            fixed4 _Main;
            fixed4 _Edge;

            float4 _MainTex_ST;
            float _Fresnel;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xyz);
                half rim = 1.0 - saturate(dot(viewDirection, i.worldNormal));

                rim = lerp(1.0,rim,_Fresnel);

                fixed4 col = lerp(_Main, _Edge, rim);

                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
